import { Geolocation, Geoposition } from '@ionic-native/geolocation';
import { IonButton, IonCard, IonIcon, IonInput, IonItem, IonLabel, IonLoading, IonRefresher, IonRefresherContent, IonToast, withIonLifeCycle } from '@ionic/react';
import { saveOutline } from 'ionicons/icons';
import React, { useState } from 'react';
import Tab1 from '../pages/Tab1';

interface LocationError {
    showError: boolean;
    message?: string;
}

const GeolocationButton: React.FC = () => {
    const [loading, setLoading] = useState<boolean>(false);
    const [error, setError] = useState<LocationError>({ showError: false });
    const [position, setPosition] = useState<Geoposition>();
   

    async function getLocation() {
        setLoading(true);

        try {
            const position = await Geolocation.getCurrentPosition();
            setPosition(position);
            setLoading(false);
            setError({ showError: false });
        } catch (e) {
            setError({ showError: true, message: e.message });
            setLoading(false);
        }
    }    

    return (
        <>
            <IonLoading
                isOpen={loading}
                onDidDismiss={() => setLoading(false)}
                message={'Getting Location...'}
            />
            <IonToast
                isOpen={error.showError}
                onDidDismiss={() => setError({ message: "", showError: false })}
                message={error.message}
                duration={3000}
            />
            
      
        <IonCard >
            <IonItem>
            <IonLabel className="inputLoc">latitude:</IonLabel> 
            <IonInput name="posLatitude" id="latitude" value={position  ? `${position.coords.latitude}`:''}/>
            </IonItem>
            <IonItem>
            <IonLabel className="inputLoc">longitude:</IonLabel> 
            <IonInput name="posLatitude" id="longitude" value={position  ? `${position.coords.longitude}`:''} />
            </IonItem>            
            </IonCard>
            <IonButton slot="end" fill="outline" size="small" color="primary" onClick={getLocation} >
                <IonIcon icon={saveOutline}/>
                Get Location
            </IonButton>
            
            
        </>
    );
};

export default GeolocationButton;
import React from 'react';
import { IonCard, IonContent, IonHeader, IonInput, IonItem, IonLabel, IonPage, IonTitle, IonToolbar } from '@ionic/react';

import './main.css';

const Tab2: React.FC = () => {
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar className="IonToolbar" color="primary">
          <IonTitle  slot="">Profile</IonTitle>
        </IonToolbar>
      </IonHeader>
      
      <div className="card-align">
            <IonItem>
            <IonLabel className="inputLoc">latitude:</IonLabel> 
            <IonInput name="posLatitude" id="latitude" value=""/>
            </IonItem>
            <IonItem>
            <IonLabel className="inputLoc">longitude:</IonLabel> 
            <IonInput name="posLatitude" id="longitude" value="" />
            </IonItem>            
      </div>       
      
    </IonPage>
  );
};

export default Tab2;

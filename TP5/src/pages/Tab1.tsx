import React from 'react';
import { IonContent, IonHeader, IonInput, IonPage, IonTitle, IonToolbar } from '@ionic/react';

import './main.css';
import GeolocationButton from '../components/GeolocationButton';

const Tab1: React.FC = () => {
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar className="IonToolbar" color="primary">
          <IonTitle  slot="">live position</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Geolocation</IonTitle>
          </IonToolbar>
        </IonHeader>

        <div className="card-align">
          <IonInput readonly value="Your current location is :"/>          
          <GeolocationButton/>
        </div>
      </IonContent>
    </IonPage>
  );
};

export default Tab1;
